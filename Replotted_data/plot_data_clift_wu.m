% plot data from clift and vannucchi (2004) and wu et al (2008)

direc1 = ['data_clift_vannucchi2004.csv'];
direc2 = ['data_wu_etal2008.csv'];

% load data Clift and Vannucchi 2004
M = importdata(direc1);

A01.type          = M.data(:,1);
A01.u0            = M.data(:,2);
A01.taper_angle   = M.data(:,3);
A01.wedge_width   = M.data(:,4);
A01.hsed          = M.data(:,5);
A01.sed_deliv     = M.data(:,6);
A01.sed_subd_rate = M.data(:,7);
clearvars M

% load data Wu et al 2008
M = importdata(direc2);

A02.Rcurv          = M.data(:,1);
A02.u0             = M.data(:,2);
A02.type           = M.data(:,3);

%%%%%%% PLOTTING %%%%%%%
color02 = [0.00 0.80 1.00]; %Subdsed01
color01 = [1.00 0.60 0.00]; %Subdsed03
color03 = [0.00 0.20 0.80]; %Subdsed04

% Plot specs
font_size   = 14;
size_color  = 50;
smth_factor = 0.15;

color_purple= [0.40 0.00 0.60];
color_yell  = [1.00 0.80 0.20];

%%% 1 %%%
figure(1); clf;
hold on
grid on
box on

ind1 = find(A01.type==0);
h02 = scatter(A01.taper_angle(ind1),A01.u0(ind1),size_color,A01.type(ind1),'filled','MarkerFaceColor',color_purple);
%h02 = plot(A01.taper_angle(ind1(1)),A01.u0(ind1(1)),'o','Color',color01,'LineWidth',3);
ind2 = find(A01.type==1);
h03 = scatter(A01.taper_angle(ind2),A01.u0(ind2),size_color,A01.type(ind2),'filled','MarkerFaceColor',color_yell);
%h03 = plot(A01.taper_angle(ind2(1)),A01.u0(ind2(1)),'o','Color',color02,'LineWidth',3);

%h01 = scatter(A01.taper_angle,A01.u0,size_color,A01.type,'filled','MarkerFaceColor',color01);

% labels
xmin = 0; xmax = 20;
ymin = 0; ymax = 150;
axis([xmin xmax ymin ymax]);
axis square

xlabel('Taper angle (^o)','FontSize',font_size);
ylabel('Convergence rate (mm/yr)','FontSize',font_size);
% ylabel('Convergence rate (km/Myr)','FontSize',font_size); % same scale as mm/yr

hlocation = 'NorthEast';

h = legend([h02 h03],{'Erosional margin','Accretionary margin'},'Location',hlocation,'FontSize',font_size-4);

set(gca,'FontSize',font_size);

set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
Name01 = ['clift_vannucchi_2004-angle-u0.png'];

% % save figures
print(figure(1),Name01,'-dpng');

%%% 2 %%%
figure(2); clf;
hold on
grid on
box on

ind1 = find(A01.type==0);
h02 = scatter(A01.taper_angle(ind1),A01.hsed(ind1),size_color,A01.type(ind1),'filled','MarkerFaceColor',color_purple);

ind2 = find(A01.type==1);
h03 = scatter(A01.taper_angle(ind2),A01.hsed(ind2),size_color,A01.type(ind2),'filled','MarkerFaceColor',color_yell);

% labels
xmin = 0; xmax = 20;
ymin = 0; ymax = 10;
axis([xmin xmax ymin ymax]);
axis square

xlabel('Taper angle (^o)','FontSize',font_size);
ylabel('Sediment thickness (km)','FontSize',font_size);

hlocation = 'NorthEast';

h = legend([h02 h03],{'Erosional margin','Accretionary margin'},'Location',hlocation,'FontSize',font_size-4);

set(gca,'FontSize',font_size);

set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
Name02 = ['clift_vannucchi_2004-angle-hsed.png'];

% % save figures
print(figure(2),Name02,'-dpng');

%%% 3 %%%
figure(3); clf;
hold on
grid on
box on

ind1 = find(A01.type==0);
h02 = scatter(A01.hsed(ind1),A01.u0(ind1),size_color,A01.type(ind1),'filled','MarkerFaceColor',color_purple);

ind2 = find(A01.type==1);
h03 = scatter(A01.hsed(ind2),A01.u0(ind2),size_color,A01.type(ind2),'filled','MarkerFaceColor',color_yell);

% labels
ymin = 0; ymax = 150;
xmin = 0; xmax = 10;
axis([xmin xmax ymin ymax]);
axis square

ylabel('Convergence rate (mm/yr)','FontSize',font_size);
% ylabel('Convergence rate (km/Myr)','FontSize',font_size); % same scale as mm/yr
xlabel('Sediment thickness (km)','FontSize',font_size);

hlocation = 'NorthEast';

h = legend([h02 h03],{'Erosional margin','Accretionary margin'},'Location',hlocation,'FontSize',font_size-4);

set(gca,'FontSize',font_size);

set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
Name03 = ['clift_vannucchi_2004-hsed-u0.png'];

% % save figures
print(figure(3),Name03,'-dpng');

%%% 4 %%%
figure(4); clf;
hold on
grid on
box on

ind1 = find(A01.type==0);
h02 = scatter(A01.sed_subd_rate(ind1),A01.hsed(ind1),size_color,A01.type(ind1),'filled','MarkerFaceColor',color_purple);

ind2 = find(A01.type==1);
h03 = scatter(A01.sed_subd_rate(ind2),A01.hsed(ind2),size_color,A01.type(ind2),'filled','MarkerFaceColor',color_yell);

% labels
xmin = 0; xmax = 120;
ymin = 0; ymax = 10;
axis([xmin xmax ymin ymax]);
axis square

xlabel('Material subduction rate (km^3/Myr)','FontSize',font_size);
ylabel('Sediment thickness (km)','FontSize',font_size);

hlocation = 'NorthEast';

h = legend([h02 h03],{'Erosional margin','Accretionary margin'},'Location',hlocation,'FontSize',font_size-4);

set(gca,'FontSize',font_size);

set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
Name04 = ['clift_vannucchi_2004-hsed-sedrate.png'];

% % save figures
print(figure(4),Name04,'-dpng');

%%% 5 %%%
figure(5); clf;
hold on
grid on
box on

ind1 = find(A02.type==0);
h02 = scatter(A02.Rcurv(ind1),A02.u0(ind1),size_color,A02.type(ind1),'filled','MarkerFaceColor',color_purple);

ind2 = find(A02.type==1);
h03 = scatter(A02.Rcurv(ind2),A02.u0(ind2),size_color,A02.type(ind2),'filled','MarkerFaceColor',color_yell);

% labels
xmin = 0; xmax = 1200;
ymin = 0; ymax = 150;
axis([xmin xmax ymin ymax]);
axis square

xlabel('Radius of curvature (km)','FontSize',font_size);
ylabel('Convergence rate (mm/yr)','FontSize',font_size);

hlocation = 'NorthEast';

h = legend([h02 h03],{'Erosional margin','Accretionary margin'},'Location',hlocation,'FontSize',font_size-4);

set(gca,'FontSize',font_size);

set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
Name05 = ['wu_et_al_2008-rcurv-u0.png'];

% % save figures
print(figure(5),Name05,'-dpng');


# Rep-MSubdSed

README file for reproducing results in:  
**The effect of sediment fluxes on the dynamics and style of convergent margins**  
~submitted~

Authors: Adina E. Pusok (1), Dave R. Stegman (2), Madeleine Kerr (2)

Affiliation: 

(1) Department of Earth Sciences, University of Oxford, UK

(2) Scripps Institution of Oceanography, UCSD  

Corresponding author: adina.pusok@earth.ox.ac.uk

### Structure of Repository ###

* Publication
* Figures
* Input_Files
* Model\_Setup

### Code and dependencies ###

[LaMEM](https://bitbucket.org/bkaus/lamem/branch/cvi_test) version and dependencies (Kaus et al., 2016).
LaMEM version used: cvi\_test branch latest commit

LaMEM source code/branches are not provided in this archive! Please download them from the links provided.
LaMEM and the cvi_branch can be downloaded from Bitbucket using ‘git clone’.

The following dependencies are needed to install LaMEM and reproduce results: petsc3.7.5, gcc6 and mpi compilers (available through Macports on MAC OSX). Installation details are located in: LaMEM (cvi_test)/doc/installation, and in the LaMEM repository wiki page. Warning: newer compilers might not work with the code version, but the model setup can be easily constructed with a newer version of LaMEM.

### Model Setup ###

The model setups (i.e particle files) were created in Matlab. The Matlab scripts for this study can be found in Model\_Setup/. Executing them will produce the particle files for the different geometries needed. <br/>
Preview of initial geometry is possible in Paraview with the option:  
Paraview_output = 1;

Each simulations was run on 4 cpus on Comet HPC (SDSC).

### Input Files ###

(LaMEM Parameter Files)

Every simulation can be reproduced by running each parameter file with the respective particle input geometry.
The input geometry obtained before is specified in each file as:  
LoadInitialParticlesDirectory  = ../../Input/MatlabInputParticles

General command to run LaMEM (may change on high-performance clusters):  
mpiexec -n 4 ./*path to exec*/LaMEM -ParamFile Setup_SubdSed_2D -restart 1

### Visualization and Post-processing ###

Performed in Paraview and Matlab. Figures/ contains low-resolution output files for every simulation in this study.
Please contact the main author for questions and access regarding the post-processing work.

### Extended Archive (request from A.E.P.)###

(to be updated after publication)

% --------------------------------------------------%
%%%%% 2D SubdSed Parameters - GLADE 2018        %%%%%
% --------------------------------------------------%

% This script creates LaMEM input files (parallel and/or sequential) for markers
% Files contain: marker coordinates, phase and temperature distributions
% WARNING: The model setup should be dimensional! Non-dimensionalization is done internally in LaMEM!
% WARNING: units should be consistent with the input file
%          old_input = [m, deg C]
%          new_input (units=si ) = [m,  deg K]
%          new_input (units=geo) = [km, deg C]

clear
%addpath ./matlab

%==========================================================================
% OUTPUT OPTIONS
%==========================================================================
% See model setup in Paraview 1-YES; 0-NO
Paraview_output        = 1;

% Output a single file containing particles information for LaMEM (msetup = redundant)
LaMEM_Redundant_output = 0;

% Output parallel files for LaMEM, using a processor distribution file (msetup = parallel)
% WARNING: Need a valid 'Parallel_partition' file!
LaMEM_Parallel_output  = 0;

% Mesh from file 1-YES (load uniform or variable mesh from file); 0-NO (create new uniform mesh)
% WARNING: Need a valid 'Parallel_partition' file!
LoadMesh               = 1;

% Parallel partition file
Parallel_partition     = 'ProcessorPartitioning_1cpu_1.1.1.bin';

% RandomNoise
RandomNoise = logical(1);

% Avoid memory block
if (Paraview_output==1) & RandomNoise
    warning('Paraview output is not recommended for setups with random noise!')
    RandomNoise = logical(0);
end

% Output
Is64BIT     = logical(0); % if you are reading a 64 bit file (Juqueen)

% Initialize temperature
InitTemperature        = 0;

%==========================================================================
% DOMAIN PARAMETERS (DIMENSIONAL) [km, deg C]
%==========================================================================
W       =   6000;
L       =   10;
H       =   1560;

% Markers = nel_x*npart_x
nump_x  =   512*3;  %for simulations
nump_y  =   2*3;
nump_z  =   256*3;

% No of particles in a grid cell
npart_x = 3;
npart_y = 3;
npart_z = 3;

% Model specific parameters
dx  =   W/(nump_x);
dy  =   L/(nump_y);
dz  =   H/(nump_z);
x_left  = -3000;
y_front =     0;
z_bot   = -1500;

% Thicknesses
ThicknessAir        =   60; % air
ThicknessOL         =   80; % oceanic lith
ThicknessWC         =   15; % weak crust
ThicknessSC         =   20; % strong core

%%% SEDIMENTS
ThicknessSed        =    5; % sediments

% Subduction trench location
xs1 = -100;

% Unattach plates
margin = 500; % [km] unattached margin

% Angle of subduction
alpha       =  70; 

% Radius of curvature
radius      = 150; % whole radius

% Slab
slab_depth  = 200; % 150, 300

% Marker positions -  for calculating convergence
xmark1 = -2250;
xmark2 =  2250;

mark_thick = 80;

% Phase transitions
depth_lowermantle = 660; % depth to lower mantle (km)

%==========================================================================
% MESH GRID
%==========================================================================

% Create new uniform grid
if LoadMesh == 0
    x = [x_left  + dx*0.5 : dx : x_left+W  - dx*0.5 ];
    y = [y_front + dy*0.5 : dy : y_front+L - dy*0.5 ];
    z = [z_bot   + dz*0.5 : dz : z_bot+H   - dz*0.5 ];
    [X,Y,Z] =   meshgrid(x,y,z);
    [Xq,Yq] =   meshgrid(x,y);
end

% Load grid from parallel partitioning file
if LoadMesh == 1
    [Xreg,Yreg,Zreg,x,y,z,X,Y,Z] = FDSTAGMeshGeneratorMatlab(npart_x,npart_y,npart_z,Parallel_partition, RandomNoise,Is64BIT );
    [Xq,Yq] = meshgrid(Xreg(1,:,1),Yreg(:,1,1));
    
    % Update other variables
    nump_x = size(Xreg,2);
    nump_y = size(Xreg,1);
    nump_z = size(Xreg,3);
end

%==========================================================================
% PHASES
%==========================================================================

%PHASES
mantle        = 0;
air           = 1;

% subducting slab
slab          = 2;
strong_core   = 3;
weak_crust    = 4;
sediments     = 5;

% upper plate
slabUP        = 6;
strong_coreUP = 7;

% other
lower_mantle  = 8;

% markers
mark1         = 9;
mark2         = 10;

% continental Upper Plate
crustUP       = 11;

% Initialize phases structure
Phase   =   zeros(size(X));     %   Contains phases

%==========================================================================
% SETUP GEOMETRY
%==========================================================================

% 1. Create Plates
ind         =   find(Z>(H-ThicknessAir-ThicknessOL+z_bot));
Phase(ind)  =   slabUP;

DepthSC     =   ThicknessOL/2;
ind         =   find(Z>(H-ThicknessAir-(DepthSC+ThicknessSC/2)+z_bot) & Z<(H-ThicknessAir-(DepthSC-ThicknessSC/2)+z_bot));
Phase(ind)  =   strong_coreUP;

% Continental Upper plate
ind         =   find(Z>(H-ThicknessAir-ThicknessWC+z_bot));
Phase(ind)  =   crustUP;

% 2. Subduction
radiusS     =   radius-ThicknessOL;
radiusWC    =   radius-ThicknessWC;
radiusSed   =   radius-ThicknessSed;

xcenter     =   xs1;
zcenter     =   H-ThicknessAir-radius+z_bot;

%%% SLAB %%%
% points A,B of the hinge at alpha angle
xA = xcenter + cosd(90-alpha)*radiusS;
zA = zcenter + sind(90-alpha)*radiusS;

xB = xcenter + cosd(90-alpha)*radius;
zB = zcenter + sind(90-alpha)*radius;

m = (zA-zB)/(xA-xB);
b = zA - m*xA;

% hinge
ind         =   find( (X > xcenter) & (Z>m*X+b) & ((X-xcenter).^2+(Z-zcenter).^2<radius^2) & ((X-xcenter).^2+(Z-zcenter).^2>radiusS^2));
Phase(ind)  =   slab;

% downgoing part
ind         =   find( (X >= xA) & Z>=(H-ThicknessAir-slab_depth+z_bot) & (zA-Z)<=(X-xA)*tand(alpha) & Z<=zB & (zB-Z)>=(X-xB)*tand(alpha));
Phase(ind)  =   slab;

%%% WEAK CRUST %%%
% points A,B of the hinge at alpha angle
xA = xcenter + cosd(90-alpha)*radiusWC;
zA = zcenter + sind(90-alpha)*radiusWC;

xB = xcenter + cosd(90-alpha)*radius;
zB = zcenter + sind(90-alpha)*radius;

m = (zA-zB)/(xA-xB);
b = zA - m*xA;

% hinge
ind         =   find( (X > xcenter) & (Z>m*X+b) & ((X-xcenter).^2+(Z-zcenter).^2<radius^2) & ((X-xcenter).^2+(Z-zcenter).^2>radiusWC^2));
Phase(ind)  =   weak_crust;

% downgoing part
ind         =   find( (X >= xA) & Z>=(H-ThicknessAir-slab_depth+z_bot) & (zA-Z)<=(X-xA)*tand(alpha) & Z<=zB & (zB-Z)>=(X-xB)*tand(alpha));
Phase(ind)  =   weak_crust;

%%% SEDIMENTS %%%
% points A,B of the hinge at alpha angle
xA = xcenter + cosd(90-alpha)*radiusSed;
zA = zcenter + sind(90-alpha)*radiusSed;

xB = xcenter + cosd(90-alpha)*radius;
zB = zcenter + sind(90-alpha)*radius;

m = (zA-zB)/(xA-xB);
b = zA - m*xA;

% hinge
ind         =   find( (X > xcenter) & (Z>m*X+b) & ((X-xcenter).^2+(Z-zcenter).^2<radius^2) & ((X-xcenter).^2+(Z-zcenter).^2>radiusSed^2));
Phase(ind)  =   sediments;

% downgoing part
ind         =   find( (X >= xA) & Z>=(H-ThicknessAir-slab_depth+z_bot) & (zA-Z)<=(X-xA)*tand(alpha) & Z<=zB & (zB-Z)>=(X-xB)*tand(alpha));
Phase(ind)  =   sediments;


%%% STRONG CORE %%%
% points A,B of the hinge at alpha angle
xA = xcenter + cosd(90-alpha)*(radius-ThicknessOL/2-ThicknessSC/2);
zA = zcenter + sind(90-alpha)*(radius-ThicknessOL/2-ThicknessSC/2);

xB = xcenter + cosd(90-alpha)*(radius-ThicknessOL/2+ThicknessSC/2);
zB = zcenter + sind(90-alpha)*(radius-ThicknessOL/2+ThicknessSC/2);

m = (zA-zB)/(xA-xB);
b = zA - m*xA;

% hinge
radius1     =   radius-ThicknessOL/2-ThicknessSC/2;
radius2     =   radius-ThicknessOL/2+ThicknessSC/2;
ind         =   find( (X > xcenter) & (Z>m*X+b) & ((X-xcenter).^2+(Z-zcenter).^2<radius2^2) & ((X-xcenter).^2+(Z-zcenter).^2>radius1^2));
Phase(ind)  =   strong_core;

% downgoing part
ind         =   find( (X >= xA) & Z>=(H-ThicknessAir-slab_depth+z_bot) & (zA-Z)<=(X-xA)*tand(alpha) & Z<=zB & (zB-Z)>=(X-xB)*tand(alpha));
Phase(ind)  =   strong_core;

%%% HORIZONTAL PLATES
ind         =   find(Z>(H-ThicknessAir-ThicknessOL+z_bot) & X<=xs1);
Phase(ind)  =   slab;

ind         =   find(Z>(H-ThicknessAir-ThicknessWC+z_bot) & X<=xs1);
Phase(ind)  =   weak_crust;

ind         =   find(Z>(H-ThicknessAir-ThicknessSed+z_bot) & X<=xs1);
Phase(ind)  =   sediments;

DepthSC     =   ThicknessOL/2;
ind         =   find(Z>(H-ThicknessAir-(DepthSC+ThicknessSC/2)+z_bot) & Z<(H-ThicknessAir-(DepthSC-ThicknessSC/2)+z_bot)& X<=xs1);
Phase(ind)  =   strong_core;

% Add left margin - detach plate
ind         =   find(X < x_left+margin);
Phase(ind)  =   mantle;

% % Add right margin - detach plate
% ind         =   find(X > x_left+W-margin);
% Phase(ind)  =   mantle;

% correct weak crust
ind         =   find(Z>(H-ThicknessAir-ThicknessWC+z_bot) & X<=xmark1+mark_thick);
Phase(ind)  =   slab;

% add triangle to plates - left
tanalpha    =   ThicknessOL/abs(xmark1-x_left-margin);
ind         =   find( (Z<((H-ThicknessAir+z_bot)-((X-x_left-margin)*tanalpha))) & Z>(depth_lowermantle+z_bot));
Phase(ind)  =   mantle;

% Add Lower mantle
ind         =   find( Z<(H-ThicknessAir-depth_lowermantle+z_bot) );
Phase(ind)  =   lower_mantle;

% Add AIR
ind         =   find( Z>(H-ThicknessAir+z_bot) );
Phase(ind)  =   air;

% Insert markers in the lower crust
ind         =   find(Phase==strong_core   & X>=xmark1 & X<xmark1+mark_thick);
Phase(ind)  =   mark1;

ind         =   find(Phase==strong_coreUP & X>=xmark2 & X<xmark2+mark_thick);
Phase(ind)  =   mark2;

%==========================================================================
% TEMPERATURE - in Celcius
%==========================================================================
% Set initial temperature distribution (air) - in Celcius
Temp    =   zeros(size(X));

%==========================================================================
% PREPARE DATA FOR VISUALIZATION/OUTPUT
%==========================================================================

% Prepare data for visualization/output
A = struct('W',[],'L',[],'H',[],'nump_x',[],'nump_y',[],'nump_z',[],'Phase',[],'Temp',[],'x',[],'y',[],'z',[],'npart_x',[],'npart_y',[],'npart_z',[]);

Phase       = permute(Phase,[2 1 3]);
Temp        = permute(Temp, [2 1 3]);

% Linear vectors containing coords
x = X(1,:,1);
y = Y(:,1,1);
z = Z(1,1,:);

A.W      = W;
A.L      = L;
A.H      = H;
A.nump_x = nump_x;
A.nump_y = nump_y;
A.nump_z = nump_z;
A.Phase  = Phase;
A.Temp   = Temp;
A.x      = x(:);
A.y      = y(:);
A.z      = z(:);
A.npart_x= npart_x;
A.npart_y= npart_y;
A.npart_z= npart_z;

X        = permute(X,[2 1 3]);
Y        = permute(Y,[2 1 3]);
Z        = permute(Z,[2 1 3]);

A.Xpart  =  X;
A.Ypart  =  Y;
A.Zpart  =  Z;

% SAVE DATA IN 1 FILE (redundant)
if (LaMEM_Redundant_output == 1)
    PhaseVec(1) = nump_z;
    PhaseVec(2) = nump_y;
    PhaseVec(3) = nump_x;
    PhaseVec    = [PhaseVec(:); X(:); Y(:); Z(:); Phase(:); Temp(:)];
    
    % Save data to file
    ParticleOutput  =   'MarkersInput2D.dat';
    
    PetscBinaryWrite(ParticleOutput, PhaseVec);
    
end

% Clearing up some memory for parallel partitioning
clearvars -except A Paraview_output LaMEM_Parallel_output Parallel_partition Is64BIT RandomNoise

% PARAVIEW VISUALIZATION
if (Paraview_output == 1)
    if (RandomNoise)
        FDSTAGWriteMatlab2VTK(A,'VTU_BINARY'); % vtu binary for markers
    else
        FDSTAGWriteMatlab2VTK(A,'BINARY'); % default option - for regular mesh
    end
    %FDSTAGWriteMatlab2VTK(A,'ASCII'); % for debugging only (slow)
end

% SAVE PARALLEL DATA (parallel)
if (LaMEM_Parallel_output == 1)
    FDSTAGSaveMarkersParallelMatlab(A,Parallel_partition,Is64BIT);
end

%clear data
clear
